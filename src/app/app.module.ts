import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AppPanxMapComponent } from "./components/shared/panx-map/panx-map.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HeaderComponent } from "./layout/header/header.component";
import { FooterComponent } from "./layout/footer/footer.component";
import { BodyComponent } from "./layout/body/body.component";
import { InfoSidebarComponent } from "./components/info-sidebar/info-sidebar.component";
import { MusicPlayerComponent } from "./components/shared/music-player/music-player.component";
import { FilterContentComponent } from "./components/shared/filter-content/filter-content.component";
import { MaterialModule } from "./material/material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    AppPanxMapComponent,
    HeaderComponent,
    FooterComponent,
    BodyComponent,
    InfoSidebarComponent,
    MusicPlayerComponent,
    FilterContentComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "serverApp" }),
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
