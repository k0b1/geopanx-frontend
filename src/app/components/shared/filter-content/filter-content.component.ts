import { Component, OnInit, ViewChild } from "@angular/core";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-filter-content",
  templateUrl: "./filter-content.component.html",
  styleUrls: ["./filter-content.component.css"],
})
export class FilterContentComponent implements OnInit {
  options: string[] = ["Banja Luka", "Beograd", "Vinkovci"];
  @ViewChild("filterForm", { static: true }) form;
  cities = new FormControl("Banja Luka");
  timePeriod = new FormControl("sada");
  bands = new FormControl("Deer in the headlights");
  country = new FormControl("Yugoslavia");
  constructor() {}

  ngOnInit() {
    console.log(this.options);
    console.log("filteri: ", this.cities);
    this.cities.valueChanges.subscribe((values) => {
      console.log("values", values);
    });
  }

  whenFiltering(event: string, type: string) {
    switch (type) {
      case "cities":
        console.log(
          "Filterisem po gradu, uzimajucu u obzir ostale aktivne filtere!",
          event
        );
        break;
      case "period":
        console.log(
          "Filterisem po periodu, uzimajucu u obzir ostale aktivne filtere!",
          event
        );
        break;
      case "country":
        console.log(
          "Filterisem po zemlji, uzimajucu u obzir ostale aktivne filtere!",
          event
        );
        break;
      case "bands":
        console.log(
          "Filterisem po bendu, uzimajucu u obzir ostale aktivne filtere!",
          event
        );
        console.log(event);
        break;
    }
  }
}
