import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanxMapComponent } from './panx-map.component';

describe('PanxMapComponent', () => {
  let component: PanxMapComponent;
  let fixture: ComponentFixture<PanxMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanxMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanxMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
