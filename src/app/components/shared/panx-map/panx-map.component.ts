import { Component, OnInit, AfterViewInit } from "@angular/core";
import * as L from "leaflet";
import { PanxMapService } from "src/app/services/panx-map/panx-map.service";

@Component({
  selector: "app-panx-map",
  templateUrl: "./panx-map.component.html",
  styleUrls: ["./panx-map.component.scss"],
  providers: [PanxMapService]
})
export class AppPanxMapComponent implements AfterViewInit {
  private map;

  constructor(private bandData: PanxMapService) {}

  ngOnInit() {
    //    console.log(this.bandData.getBandsData()[0].data.bands);
  }

  private defaultIcon: L.Icon = L.icon({
    iconUrl: "assets/images/punk-marker.png",
    //    shadowUrl: 'assets/leaflet/marker-shadow.png'
    //   iconSize: [41, 51], // => random values you have to choose right ones for your case
    iconAnchor: [15, 20.9] // => random values too
  });

  private initMap(): void {
    L.Marker.prototype.options.icon = this.defaultIcon;

    this.map = L.map("map").setView([43.866667, 18.416667], 7);
    const tiles = L.tileLayer(
      "https://tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png",
      {
        maxZoom: 20,
        attribution:
          '&copy; <a href="https://stadiamaps.com/"> Stadia Maps</a> &copy; <a href="https://openmaptiles.org/"> OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
      }
    );
    for (let band of this.bandData.getBandsData()[0].data.bands) {
      L.marker(band.geolocation)
        .addTo(this.map)
        .bindPopup(
          "<div style='margin-bottom: 30px'>" +
            band.name +
            "<br> " +
            band.about +
            "</div>"
        );
      //.openPopup(false);
    }

    tiles.addTo(this.map);
  }

  ngAfterViewInit(): void {
    this.initMap();
  }
}
