import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatListModule } from "@angular/material/list";
import { MatDividerModule } from "@angular/material/divider";
import { MatMenuModule } from "@angular/material/menu";
import { MatIconModule } from "@angular/material/icon";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatInputModule } from "@angular/material/input";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatListModule,
    MatDividerModule,
    MatMenuModule,
    MatIconModule,
    MatAutocompleteModule,
    MatInputModule
  ],
  exports: [
    MatMenuModule,
    MatListModule,
    MatDividerModule,
    MatMenuModule,
    MatIconModule,
    MatAutocompleteModule,
    MatInputModule
  ]
})
export class MaterialModule {}
