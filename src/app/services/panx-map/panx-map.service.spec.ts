import { TestBed } from '@angular/core/testing';

import { PanxMapService } from './panx-map.service';

describe('PanxMapService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PanxMapService = TestBed.get(PanxMapService);
    expect(service).toBeTruthy();
  });
});
