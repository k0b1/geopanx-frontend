import { Injectable } from "@angular/core";
//import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class PanxMapService {
  baseUrl: string = "";

  constructor() {}

  getBandsData() {
    // this.http.get(this.baseUrl + "/bands-data").subscribe( res => {
    //     console.log("Clicked band data: ", res)
    // })
    return [
      {
        data: {
          bands: [
            {
              name: "Revolt",
              active: "1995-",
              period: ["1990", "2000", "2010"],
              members: "Tica, Krema...",
              style: "hardcore, rap, metal, punk",
              geolocation: [44.766667, 17.183333],
              about: "Nastao je tada i svirao je okolo",
              releases: ["U kuci bola"],
              music: "/Revolt",
              interview: "",
              related: "",
              from: ""
            },
            {
              name: "Dear in the headlights",
              active: "2010-",
              period: ["2010"],
              members: "Zule, Igor...",
              style: "hardcore, metal, punk, emo",
              geolocation: [44.786667, 17.183333],
              about: "Nastao je tada i svirao po Evropi",
              releases: ["U kuci bola"],
              music: "/Revolt",
              interview: "",
              related: "",
              from: ""
            },
            {
              name: "Solunski front",
              active: "1981-1985",
              period: ["1980"],
              members: "neki clanovi...",
              style: "hardcore, punk",
              geolocation: [44.816667, 20.466667],
              about: "Nastao je u Beogradu i svirao kao ded kenedis",
              releases: ["dva demo snimka"],
              music: "/solunski-front",
              interview: "",
              related: "",
              from: ""
            },
            {
              name: "Partizanska eskadrila",
              active: "1993-???",
              period: ["1990"],
              members: "neki clanovi...",
              style: "hardcore, punk",
              geolocation: [44.983333, 19.616667],
              about: "Nastao je u Beogradu i svirao kao ded kenedis",
              releases: ["dva demo snimka"],
              music: "/solunski-front",
              interview: "",
              related: "",
              from: ""
            }
          ]
        }
      }
    ];
  }
}
